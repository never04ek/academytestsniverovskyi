using System;
using Newtonsoft.Json;

namespace Models
{
    public class Team : IEntity
    {
        [JsonProperty("id")] public int Id { get; set; }

        [JsonProperty("name")]public string Name { get; set; }
        [JsonProperty("created_at")] public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return
                $"Team = {{id : {Id}, \nName : {Name}, \nCreatedAt : {CreatedAt}}}";
        }
    }
}