using Newtonsoft.Json;

namespace Models
{
    public interface IEntity
    {
         int Id { get; set; }
    }
}