using Microsoft.EntityFrameworkCore;
using Models;

namespace BusinessLogic.Tests.Fake
{
    public partial class FakeAcademyDbContext : DbContext
    {
        public FakeAcademyDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=/Users/nikita/RiderProjects/AcademyTestsNiverovskyi/BusinessLogic.Tests/test.db");
        }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }
    }
}