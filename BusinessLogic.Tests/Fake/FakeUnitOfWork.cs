using System.Threading.Tasks;
using DataAccess.Repository;
using DataAccess.UnitOfWork;
using Models;

namespace BusinessLogic.Tests.Fake
{
    public class FakeUnitOfWork : IUnitOfWork
    {

        public void Dispose()
        {
        }

        public IRepository<T> Repository<T>() where T : class, IEntity
        {
            return FakeDb<T>.Repository();
        }
        
        

        public Task SaveChangesAsync()
        {
            return Task.CompletedTask;
        }
    }
}