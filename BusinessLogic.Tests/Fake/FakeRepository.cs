using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Tests.Fake
{
    public class FakeRepository<T> : IRepository<T> where T : class, IEntity
    {
        private List<T> _entities;

        public FakeRepository(List<T> entities)
        {
            _entities = entities;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult( _entities.AsEnumerable());
        }

        public IQueryable<T> Query()
        {
            return _entities.AsQueryable();
        }

        public void Add(T entity)
        {
            _entities.Add(entity);
        }

        public IEnumerable<T> GetAll()
        {
            return _entities;
        }

        public void Delete(T entity)
        {
            _entities.Remove(entity);
        }

        public Task<IEnumerable<T>> GetAsync(Expression<Func<T, bool>> expression)
        {
            return Task.FromResult( _entities.Where(expression.Compile()).AsEnumerable());
        }

        public Task<T> GetAsync(int id)
        {
            return Task.FromResult(_entities.FirstOrDefault(e => e.Id == id));
        }

        public void Update(T entity)
        {
            var index = _entities.FindIndex(e => e.Id == entity.Id);
            _entities[index] = entity;
        }
    }
}