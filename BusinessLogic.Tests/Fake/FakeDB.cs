using System.Collections.Generic;
using DataAccess.Repository;
using Models;

namespace BusinessLogic.Tests.Fake
{
    public static class FakeDb<T> where T:class,IEntity
    {
        private static List<T> _entities;


        public static IRepository<T> Repository()
        {
            if(_entities == null)
                _entities = new List<T>();
            return new FakeRepository<T>(_entities);
        }
    }
}