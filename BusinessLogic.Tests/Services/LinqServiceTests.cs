using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Services;
using DataAccess.Repository;
using DataAccess.UnitOfWork;
using Models;
using NUnit.Framework;
using FakeItEasy;

namespace BusinessLogic.Tests.Services
{
    public class LinqServiceTests
    {
        private LinqService _linqService;
        private readonly List<User> _users = new List<User>();
        private readonly List<Tasks> _tasks = new List<Tasks>();
        private readonly List<Team> _teams = new List<Team>();
        private readonly List<Project> _projects = new List<Project>();

        [OneTimeSetUp]
        public void Setup()
        {
            var userRepository = A.Fake<IRepository<User>>();
            var tasksRepository = A.Fake<IRepository<Tasks>>();
            var teamRepository = A.Fake<IRepository<Team>>();
            var projectRepository = A.Fake<IRepository<Project>>();
            A.CallTo(() => userRepository.GetAllAsync()).Returns(_users);
            A.CallTo(() => tasksRepository.GetAllAsync()).Returns(_tasks);
            A.CallTo(() => teamRepository.GetAllAsync()).Returns(_teams);
            A.CallTo(() => projectRepository.GetAllAsync()).Returns(_projects);

            var unitOfWork = A.Fake<IUnitOfWork>();
            A.CallTo(() => unitOfWork.Repository<User>()).Returns(userRepository);
            A.CallTo(() => unitOfWork.Repository<Tasks>()).Returns(tasksRepository);
            A.CallTo(() => unitOfWork.Repository<Team>()).Returns(teamRepository);
            A.CallTo(() => unitOfWork.Repository<Project>()).Returns(projectRepository);

            _linqService = new LinqService(unitOfWork);
        }

        [TearDown]
        public void Teardown()
        {
            _projects.Clear();
            _tasks.Clear();
            _teams.Clear();
            _users.Clear();
        }

        [Test]
        public async Task GetAmountOfUserTasksInEachProject_When_give_value_Then_get_specific_result()
        {
            //Arrange

            #region AddProjects

            var p1 = new Project
            {
                Id = 1,
            };
            _projects.Add(p1);
            _projects.Add(new Project
            {
                Id = 2,
            });
            _projects.Add(new Project
            {
                Id = 3,
            });

            #endregion

            #region AddTasks

            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                PerformerId = 23
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                PerformerId = 23
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                PerformerId = 32
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                PerformerId = 43
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 3,
                PerformerId = 23
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                PerformerId = 23
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                PerformerId = 23
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                PerformerId = 32
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                PerformerId = 1
            });

            #endregion

            //Act
            var dic = _linqService.GetAmountOfUserTasksInEachProject(23);

            //Assert
            Assert.That((await dic)[p1], Is.EqualTo(3));
        }

        [Test]
        public async Task GetUserTasks45Name_When_give_value_Then_get_specific_result()
        {
            //Arrange

            #region AddTasks

            _tasks.Add(new Tasks
            {
                Name = "Quia animi quibusdam tempore.",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi omnis sint iste facanimi omnis sint iste facilisilis quibusdam tempore.",
                PerformerId = 3
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi omne facilis quibusdam tempore.",
                PerformerId = 1
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi omnis sint isquibusdam tempore.",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi omnanimi omnis sint iste faciliss quibusdam tempore.",
                PerformerId = 3
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi omnis squibusdam tempore.",
                PerformerId = 1
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi omnisanimi omnis sint iste facilisis quibusdam tempore.",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi omnisacilis quibusdam tempore.",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi omnis sint iste facilis sint iste facilis quibusdam tempore.",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Quia animi oanimi omnis sint iste facilite facilis quibusdam tempore.",
                PerformerId = 3
            });

            #endregion

            //Act
            var res = await _linqService.GetUserTasks45Name(2);

            //Assert
            Assert.That(res.Count, Is.EqualTo(3));
        }

        [Test]
        public async Task GetThisYearTasksOfUser_When_give_value_Then_get_specific_result()
        {
            //Arrange

            #region AddTask

            _tasks.Add(new Tasks
            {
                PerformerId = 1,
                FinishedAt = DateTime.Today
            });
            _tasks.Add(new Tasks
            {
                PerformerId = 1,
                FinishedAt = DateTime.Today.AddYears(-2)
            });
            _tasks.Add(new Tasks
            {
                PerformerId = 2,
                FinishedAt = DateTime.Today
            });
            _tasks.Add(new Tasks
            {
                PerformerId = 2,
                FinishedAt = DateTime.Today.AddYears(-2)
            });
            _tasks.Add(new Tasks
            {
                PerformerId = 1,
                FinishedAt = DateTime.Today
            });
            _tasks.Add(new Tasks
            {
                PerformerId = 1,
                FinishedAt = DateTime.Today.AddYears(-2)
            });
            _tasks.Add(new Tasks
            {
                PerformerId = 2,
                FinishedAt = DateTime.Today.AddYears(-2)
            });
            _tasks.Add(new Tasks
            {
                PerformerId = 2,
                FinishedAt = DateTime.Today.AddYears(-2)
            });

            #endregion

            //Act

            var res = await _linqService.GetThisYearTasksOfUser(1);

            //Assert
            Assert.That(res.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task GetTeamsAndUsers_When_give_value_Then_get_specific_result()
        {
            //Arrange

            #region AddUsers

            _users.Add(new User
            {
                TeamId = 1,
                Birthday = DateTime.Now.AddYears(-19),
                TimeOfRegistration = DateTime.Now.AddMonths(-2)
            });
            _users.Add(new User
            {
                TeamId = 1,
                Birthday = DateTime.Now.AddYears(-20),
                TimeOfRegistration = DateTime.Now.AddMonths(-3)
            });
            _users.Add(new User
            {
                TeamId = 2,
                Birthday = DateTime.Now.AddYears(-20),
                TimeOfRegistration = DateTime.Now.AddMonths(-10)
            });
            _users.Add(new User
            {
                TeamId = 2,
                Birthday = DateTime.Now.AddYears(-36),
                TimeOfRegistration = DateTime.Now.AddMonths(-22)
            });
            _users.Add(new User
            {
                TeamId = 3,
                Birthday = DateTime.Now.AddYears(-10),
                TimeOfRegistration = DateTime.Now.AddMonths(-1)
            });
            _users.Add(new User
            {
                TeamId = 3,
                Birthday = DateTime.Now.AddYears(-8),
                TimeOfRegistration = DateTime.Now.AddMonths(-2)
            });
            _users.Add(new User
            {
                TeamId = 1,
                Birthday = DateTime.Now.AddYears(-15),
                TimeOfRegistration = DateTime.Now.AddMonths(-5)
            });
            _users.Add(new User
            {
                TeamId = 1,
                Birthday = DateTime.Now.AddYears(-22),
                TimeOfRegistration = DateTime.Now.AddMonths(-12)
            });
            _users.Add(new User
            {
                TeamId = 2,
                Birthday = DateTime.Now.AddYears(-16),
                TimeOfRegistration = DateTime.Now.AddMonths(-10)
            });

            #endregion

            #region AddTeams

            _teams.Add(new Team
            {
                Id = 1,
                Name = "1"
            });
            _teams.Add(new Team
            {
                Id = 2,
                Name = "2"
            });
            _teams.Add(new Team
            {
                Id = 3,
                Name = "3"
            });

            #endregion

            //Act

            var res = (await _linqService.GetTeamsAndUsers()).ToList();

            //Assert

            Assert.That(res.SingleOrDefault(team => team.Id == 2).Users.Count, Is.EqualTo(3));
        }

        [Test]
        public async Task GetUserAndTasks_When_give_value_Then_get_specific_result()
        {
            //Arrange

            #region AddTasks

            _tasks.Add(new Tasks
            {
                Name = "Task1",
                PerformerId = 1
            });
            _tasks.Add(new Tasks
            {
                Name = "Task2",
                PerformerId = 1
            });
            _tasks.Add(new Tasks
            {
                Name = "Task3",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Task4",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Task5",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Task6",
                PerformerId = 3
            });
            _tasks.Add(new Tasks
            {
                Name = "Task7",
                PerformerId = 4
            });
            _tasks.Add(new Tasks
            {
                Name = "Task9",
                PerformerId = 4
            });
            _tasks.Add(new Tasks
            {
                Name = "Task10",
                PerformerId = 1
            });
            _tasks.Add(new Tasks
            {
                Name = "Task11",
                PerformerId = 2
            });
            _tasks.Add(new Tasks
            {
                Name = "Task12",
                PerformerId = 3
            });
            _tasks.Add(new Tasks
            {
                Name = "Task13",
                PerformerId = 3
            });

            #endregion

            #region AddUsers

            _users.Add(new User
            {
                Id = 1
            });
            _users.Add(new User
            {
                Id = 2
            });
            _users.Add(new User
            {
                Id = 3
            });
            _users.Add(new User
            {
                Id = 4
            });

            #endregion

            //Act

            var res = (await _linqService.GetUserAndTasks()).ToList();

            //Assert
            Assert.That(res.SingleOrDefault(user => user.User.Id == 1).Tasks.Count(), Is.EqualTo(3));
        }

        [Test]
        public async Task GetSpecUserStruct_When_give_value_Then_get_specific_result()
        {
            //Arrange

            #region AddUsers

            _users.Add(new User
            {
                Id = 1
            });
            _users.Add(new User
            {
                Id = 2
            });
            _users.Add(new User
            {
                Id = 3
            });
            _users.Add(new User
            {
                Id = 4
            });

            #endregion

            #region AddProjects

            _projects.Add(new Project
            {
                AuthorId = 3,
                CreatedAt = DateTime.Now.AddMonths(-12),
                Id = 1
            });
            _projects.Add(new Project
            {
                AuthorId = 1,
                CreatedAt = DateTime.Now.AddMonths(-11),
                Id = 2
            });
            _projects.Add(new Project
            {
                AuthorId = 2,
                CreatedAt = DateTime.Now.AddMonths(-10),
                Id = 3
            });
            _projects.Add(new Project
            {
                AuthorId = 2,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Id = 4
            });
            _projects.Add(new Project
            {
                AuthorId = 1,
                CreatedAt = DateTime.Now.AddMonths(-4),
                Id = 5
            });
            _projects.Add(new Project
            {
                AuthorId = 2,
                CreatedAt = DateTime.Now.AddMonths(-21),
                Id = 6
            });

            #endregion

            #region AddTasks

            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                PerformerId = 1,
                State = TaskState.Created,
                FinishedAt = DateTime.Now.AddMonths(-2),
                CreatedAt = DateTime.Now.AddMonths(-21)
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 3,
                PerformerId = 3,
                State = TaskState.Canceled,
                FinishedAt = DateTime.Now.AddMonths(-4),
                CreatedAt = DateTime.Now.AddMonths(-12),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                PerformerId = 4,
                State = TaskState.Finished,
                FinishedAt = DateTime.Now.AddMonths(-5),
                CreatedAt = DateTime.Now.AddMonths(-11),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 4,
                PerformerId = 1,
                State = TaskState.Canceled,
                FinishedAt = DateTime.Now.AddMonths(-1),
                CreatedAt = DateTime.Now.AddMonths(-4),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                PerformerId = 2,
                State = TaskState.Started,
                FinishedAt = DateTime.Now.AddMonths(-12),
                CreatedAt = DateTime.Now.AddMonths(-20),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 6,
                PerformerId = 3,
                State = TaskState.Canceled,
                FinishedAt = DateTime.Now.AddMonths(-10),
                CreatedAt = DateTime.Now.AddMonths(-12),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                PerformerId = 4,
                State = TaskState.Finished,
                FinishedAt = DateTime.Now.AddMonths(-7),
                CreatedAt = DateTime.Now.AddMonths(-19),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 6,
                PerformerId = 1,
                State = TaskState.Created,
                FinishedAt = DateTime.Now.AddMonths(-12),
                CreatedAt = DateTime.Now.AddMonths(-16),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                PerformerId = 3,
                State = TaskState.Finished,
                FinishedAt = DateTime.Now.AddMonths(-18),
                CreatedAt = DateTime.Now.AddMonths(-27),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 4,
                PerformerId = 4,
                State = TaskState.Canceled,
                FinishedAt = DateTime.Now.AddMonths(-5),
                CreatedAt = DateTime.Now.AddMonths(-9),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                PerformerId = 1,
                State = TaskState.Started,
                FinishedAt = DateTime.Now.AddMonths(-2),
                CreatedAt = DateTime.Now.AddMonths(-3),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                PerformerId = 4,
                State = TaskState.Finished,
                FinishedAt = DateTime.Now.AddMonths(-1),
                CreatedAt = DateTime.Now.AddMonths(-11),
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 4,
                PerformerId = 3,
                State = TaskState.Created,
                FinishedAt = DateTime.Now.AddMonths(-22),
                CreatedAt = DateTime.Now.AddMonths(-32),
            });

            #endregion

            //Act

            var res = await _linqService.GetSpecUserStruct(2);

            //Assert
            Assert.That(res.Project.Id, Is.EqualTo(4));
        }

        [Test]
        public async Task GetSpecProjectStruct_When_give_value_Then_get_specific_result()
        {
            //Arrange

            #region AddUsers

            _users.Add(new User
            {
                TeamId = 2
            });
            _users.Add(new User
            {
                TeamId = 3
            });
            _users.Add(new User
            {
                TeamId = 2
            });
            _users.Add(new User
            {
                TeamId = 1
            });
            _users.Add(new User
            {
                TeamId = 3
            });
            _users.Add(new User
            {
                TeamId = 1
            });
            _users.Add(new User
            {
                TeamId = 1
            });
            _users.Add(new User
            {
                TeamId = 2
            });
            _users.Add(new User
            {
                TeamId = 3
            });

            #endregion

            #region AddProjects

            _projects.Add(new Project
            {
                Id = 1
            });
            _projects.Add(new Project
            {
                Id = 2
            });
            _projects.Add(new Project
            {
                Id = 3
            });

            #endregion

            #region AddTasks

            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                DescriptionTask =
                    "Libero laudantium tempore et porro et non earum laudantium tempore et porro et non earum",
                Name = "Quia animi omnis sint iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                DescriptionTask = "Libero lLibero laudantium tempore et porro et non earum et non earum",
                Name = "Quia anim iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 3,
                DescriptionTask = "Libero laudantium tempore et porro et non earum",
                Name = "Quia animi sint iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                DescriptionTask = "Libero laudaLibero laudantium tempore et porro et non earumrum",
                Name = "Quia animi omt iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                DescriptionTask =
                    "Libero laudaLibero laudantium Libero laudantium tempore et porro et non earumporro et non earumet non earum",
                Name = "Quia animi Quia animi omnis sint iste facilisnt iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                DescriptionTask = "Libero lLibero laudantium tempore et porro et non earumre et porro et non earum",
                Name = "Quia anQuia animi omnis sint iste facilisimi omnis sint iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                DescriptionTask =
                    "Libero laudantiLibero laudantium tempore et porro et non earumre et porro et non earum",
                Name = "Quia animi omnis sint iste facQuia animi omnis sint iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 3,
                DescriptionTask =
                    "Libero laudanLibero laudantium tempore et porro et non earum tempore et porro et non earum",
                Name = "Quia animi nt iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 3,
                DescriptionTask = "Libero lare et porro et non earum",
                Name = "Quia animi Quia animi omnis sint iste facilisnt iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 2,
                DescriptionTask = "Libero lLibero laudantium tempore et porro et non earumporro et non earum",
                Name = "Quia aQuia animi omnis sint iste facilis facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 1,
                DescriptionTask =
                    "Libero laudantium tempore et porro et non earumLibero laudantium tempore et porro et non earumLibero laudantium tempore et porro et non earum",
                Name = "Quia  omnis sint iste facilis"
            });
            _tasks.Add(new Tasks
            {
                ProjectId = 3,
                DescriptionTask = "Libero laudantium t mpore et porro et non earumLibero et non earum",
                Name = "Quia animi omnis sint iste facilis"
            });

            #endregion

            //Act
            var res = await _linqService.GetSpecProjectStruct(2);
            
            //Assert
            Assert.That(res.LongestTask.DescriptionTask.Length, Is.EqualTo(79));

        }
    }
}