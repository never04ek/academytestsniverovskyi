using System;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Services;
using BusinessLogic.Tests.Fake;
using DataAccess.Repository;
using DataAccess.UnitOfWork;
using Models;
using NUnit.Framework;
using FakeItEasy;

namespace BusinessLogic.Tests.Services
{
    public class ProjectServiceTests
    {
        private ProjectsService _projService;

        [SetUp]
        public void Setup()
        {
            _projService = new ProjectsService(new FakeUnitOfWork());
        }
        
        

        [Test]
        public async Task GetById_Should_return_correctly_value_When_use_method()
        {
            //Arrange
            var p1 = new Project
            {
                Id = 1,
                Name = "Vero fugiat temporibus et praesentium.",
                Description =
                    "Labore tempora quo ex voluptatem ut distinctio non.\nQuibusdam sequi molestiae.\nSit corrupti aut aut.\nReiciendis voluptatem libero fugit ut saepe.\nOmnis omnis incidunt perferendis ex autem blanditiis reiciendis veritatis et.\nUt voluptates rem eaque quia esse.",
                CreatedAt = Convert.ToDateTime("2019-07-12T00:20:40.99888+00:00"),
                Deadline = Convert.ToDateTime("2020-01-07T20:48:40.709841+00:00"),
                AuthorId = 9,
                TeamId = 5
            };
            var p2 = new Project
            {
                Id = 2,
                Name = "Quos minima quis doloremque voluptates.",
                Description = "Mollitia beatae quia quia rerum esse.\nEos et adipisci libero alias cumque ducimus",
                CreatedAt = Convert.ToDateTime("2019-07-11T17:04:23.349188"),
                Deadline = Convert.ToDateTime("2019-08-22T18:56:31.5473426"),
                AuthorId = 39,
                TeamId = 1
            };
            await _projService.AddProject(p1);
            
            //Act & Assert
            Assert.That(_projService.GetProject(1).Result, Is.EqualTo(p1));
        }


    }
}