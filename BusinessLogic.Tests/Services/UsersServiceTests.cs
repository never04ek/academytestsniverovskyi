using System;
using System.Threading.Tasks;
using BusinessLogic.Services;
using BusinessLogic.Tests.Fake;
using DataAccess.Repository;
using DataAccess.UnitOfWork;
using Models;
using NUnit.Framework;
using FakeItEasy;

namespace BusinessLogic.Tests.Services
{
    public class UsersServiceTests
    {
        private UsersService _usersService;

        [SetUp]
        public void Setup()
        {
            _usersService = new UsersService(new FakeUnitOfWork());
        }

        [Test]
        public async Task Add_Should_add_user_to_db_When_call_method_add()
        {
            //Arrange
            var repository = A.Fake<IRepository<User>>();
            var unitOfWork = A.Fake<IUnitOfWork>();
            A.CallTo(() => unitOfWork.Repository<User>()).Returns(repository);
            var usersService = new UsersService(unitOfWork);
            var u1 = new User
            {
                Id = 1,
                FirstName = "Susanna",
                LastName = "Corwin",
                Email = "Susanna_Corwin@yahoo.com",
                Birthday = Convert.ToDateTime("2017-12-13T09:32:16.8799488"),
                TimeOfRegistration = Convert.ToDateTime("2019-07-07T04:14:28.2783482"),
                TeamId = 10
            };

            //Act
            await usersService.AddUser(u1);

            //Assert
            A.CallTo(() => unitOfWork.SaveChangesAsync()).MustHaveHappenedOnceExactly();
        }

        [Test]
        public async Task AddUserToTeam_Should_add_user_to_team_When_call_method()
        {
            //Arrange
            var u1 = new User
            {
                Id = 1,
                FirstName = "Susanna",
                LastName = "Corwin",
                Email = "Susanna_Corwin@yahoo.com",
                Birthday = Convert.ToDateTime("2017-12-13T09:32:16.8799488"),
                TimeOfRegistration = Convert.ToDateTime("2019-07-07T04:14:28.2783482"),
                TeamId = null
            };
            await _usersService.AddUser(u1);


            //Act
            await _usersService.AddUserToTeam(1, 5);

            //Assert
            Assert.That(_usersService.GetUser(1).Result.TeamId, Is.EqualTo(5));
        }
    }
}