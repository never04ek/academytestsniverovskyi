using System;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Services;
using BusinessLogic.Tests.Fake;
using DataAccess.UnitOfWork;
using NUnit.Framework;
using Microsoft.EntityFrameworkCore;
using Models;

namespace BusinessLogic.Tests.Services
{
    public class IntegrationTests
    {
        UnitOfWork _unitOfWork = new UnitOfWork(new FakeAcademyDbContext(new DbContextOptions<FakeAcademyDbContext>()));

        [Test]
        public async Task AddProject_Should_get_specific_value_from_db_When_add_data_to_db()
        {
            //Arrange

            var ps = new ProjectsService(_unitOfWork);

            //Act

            await ps.AddProject(new Project
            {
                Name = "Project1",
                Description = "Project1 description",
                AuthorId = 2,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddMonths(2),
                TeamId = 7
            });

            //Assert

            Assert.That((await ps.GetProjects()).SingleOrDefault(pr => pr.Name.Equals("Project1")), Is.Not.Null);
        }

        [Test]
        public async Task RemoveUser_Should_get_empty_user_table_in_db_When_remove_user()
        {
            //Arrange

            var us = new UsersService(_unitOfWork);
            await us.AddUser(new User
            {
                FirstName = "Jerry",
                LastName = "Mouse",
                Birthday = DateTime.Now.AddYears(-10),
                Email = "LoveCheese@mousemail.com",
                TimeOfRegistration = DateTime.Now.AddMonths(-10),
                TeamId = 2
            });
            //Act

            await us.DeleteUser((await us.GetUsers()).Single(user => user.LastName == "Mouse").Id);

            //Assert

            Assert.That((await us.GetUsers()).Count(), Is.Zero);
        }

        [Test]
        public async Task AddTeam_Should_get_specific_value_from_db_When_add_data_to_db()
        {
            //Arrange

            var ts = new TeamsService(_unitOfWork);

            //Act

            await ts.AddTeam(new Team()
            {
                Name = "Team1",
                CreatedAt = DateTime.Now
            });

            //Assert

            Assert.That((await ts.GetTeams()).Single(team => true).Name, Is.EqualTo("Team1"));
        }


        [Test]
        public async Task RemoveTask_Should_get_empty_user_table_in_db_When_remove_user()
        {
            //Arrange

            var ts = new TasksService(_unitOfWork);
            await ts.AddTask(new Tasks
            {
                Name = "Task1",
                CreatedAt = DateTime.Now,
                DescriptionTask = "Task1 description",
                FinishedAt = DateTime.Now.AddDays(5),
                PerformerId = 2,
                ProjectId = 1,
                State = TaskState.Created
            });
            //Act

            await ts.DeleteTask((await ts.GetTasks()).Single(task => task.Name == "Task1").Id);

            //Assert

            Assert.That((await ts.GetTasks()).Count(), Is.Zero);
        }

        [Test]
        public async Task LinqGetThisYearTasksOfUser_When_give_value_Then_get_specific_result()

        {
            //Arrange

            var tasks = new TasksService(_unitOfWork);
            var linqService = new LinqService(_unitOfWork);

            #region AddTask

            await tasks.AddTask(new Tasks
            {
                PerformerId = 1,
                FinishedAt = DateTime.Today
            });
            await tasks.AddTask(new Tasks
            {
                PerformerId = 1,
                FinishedAt = DateTime.Today.AddYears(-2)
            });
            await tasks.AddTask(new Tasks
            {
                PerformerId = 2,
                FinishedAt = DateTime.Today
            });
            await tasks.AddTask(new Tasks
            {
                PerformerId = 2,
                FinishedAt = DateTime.Today.AddYears(-2)
            });
            await tasks.AddTask(new Tasks
            {
                PerformerId = 1,
                FinishedAt = DateTime.Today
            });
            await tasks.AddTask(new Tasks
            {
                PerformerId = 1,
                FinishedAt = DateTime.Today.AddYears(-2)
            });
            await tasks.AddTask(new Tasks
            {
                PerformerId = 2,
                FinishedAt = DateTime.Today.AddYears(-2)
            });
            await tasks.AddTask(new Tasks
            {
                PerformerId = 2,
                FinishedAt = DateTime.Today.AddYears(-2)
            });

            #endregion

            //Act

            var res = await linqService.GetThisYearTasksOfUser(1);

            //Assert
            Assert.That(res.Count, Is.EqualTo(2));
        }

        [TearDown]
        public async Task ClearDb()
        {
            var projectsService = new ProjectsService(_unitOfWork);
            var projects = await projectsService.GetProjects();
            foreach (var project in projects)
            {
                await projectsService.DeleteProject(project.Id);
            }

            var teamsService = new TeamsService(_unitOfWork);
            var teams = await teamsService.GetTeams();
            foreach (var team in teams)
            {
                await teamsService.DeleteTeam(team.Id);
            }

            var tasksService = new TasksService(_unitOfWork);
            var tasks = await tasksService.GetTasks();
            foreach (var task in tasks)
            {
                await tasksService.DeleteTask(task.Id);
            }
        }
    }
}