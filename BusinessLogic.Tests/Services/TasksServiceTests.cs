using System;
using System.Threading.Tasks;
using BusinessLogic.Services;
using BusinessLogic.Tests.Fake;
using Models;
using NUnit.Framework;
namespace BusinessLogic.Tests.Services
{
    public class TasksServiceTests
    {
        private TasksService _tasksService;

        [SetUp]
        public void Setup()
        {
            _tasksService = new TasksService(new FakeUnitOfWork());
        }

        [Test]
        public async Task TasFinishTask_Should_change_state_of_task_to_finished_When_call_method()
        {
            //Arrange
            var t1 = new Tasks
            {
                Id = 1,
                Name = "Repellendus quos voluptatem.",
                DescriptionTask = "Nulla sint iusto ut et dolorem sit mollitia nostrum.\nQui omnis unde ex culpa quasi pariatur molestiae id delectus.\nInventore voluptatem quis ea ea reiciendis voluptatem voluptatem quod.\nVoluptas nobis ducimus officia corrupti blanditiis ad voluptates.\nNulla consequatur quaerat sit eum explicabo dolore voluptatem molestias.\nQui amet illo quo.",
                CreatedAt = Convert.ToDateTime( "2019-07-16T07:05:33.1545285"),
                FinishedAt = Convert.ToDateTime("2020-05-02T07:45:03.5752304"),
                State = TaskState.Created,
                ProjectId = 85,
                PerformerId = 26
            };
            await _tasksService.AddTask(t1);

            //Act
            await _tasksService.FinishTask(1);
            
            //Assert
            Assert.That(_tasksService.GetTask(1).Result.State, Is.EqualTo(TaskState.Finished));
        }
        
    }
}