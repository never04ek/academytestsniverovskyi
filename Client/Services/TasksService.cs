using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;

namespace Client.Services
{
    public class TasksService : IService<Tasks>
    {
        private static readonly WebClient Client = new WebClient();

        private static string _path = "https://localhost:5001/api";


        public async Task<Tasks> GetAsync(int id)
        {
            var str = await Client.DownloadStringTaskAsync(new Uri($"{_path}/Tasks/{id}"));
            return JsonConvert.DeserializeObject<Tasks>(str);
        }

        public async Task<IEnumerable<Tasks>> GetAllAsync()
        {
            var str = await Client.DownloadStringTaskAsync(new Uri($"{_path}/Tasks"));
            return JsonConvert.DeserializeObject<IEnumerable<Tasks>>(str);
        }

        public async Task AddAsync(Tasks task)
        {
            await Utils.Request($"{_path}/Tasks", "POST", JsonConvert.SerializeObject(task));
        }

        public async Task UpdateAsync(int id, Tasks task)
        {
            await Utils.Request($"{_path}/Tasks/{id}", "PUT", JsonConvert.SerializeObject(task));
        }

        public async Task DeleteAsync(int id)
        {
            await Utils.Request($"{_path}/Tasks/{id}", "DELETE");
        }
    }
}