﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DataAccess;
using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;

namespace Client
{
    class Program
    {
        private static async Task MarkTask(int delay)
        {
            var id = await Requests.MarkRandomTaskWithDelay(delay);
            Console.WriteLine(id);
        }

        static async Task Main(string[] args)
        {
            await MarkTask(1000);
//            Console.WriteLine(JsonConvert.DeserializeObject<List<Project>>(await Utils.Request("https://localhost:5001/api/projects", "GET"))[0]);
            Console.ReadLine();
        }
    }
}