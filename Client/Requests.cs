using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Timers;
using Models;
using Newtonsoft.Json;

namespace Client
{
    public class Requests
    {
        private static Timer _timer;
        private static TaskCompletionSource<int> _tcs;

        public static Task<int> MarkRandomTaskWithDelay(int delay)
        {
            _timer = new Timer(delay);
            _tcs = new TaskCompletionSource<int>();
            _timer.Start();
            _timer.Elapsed += OnTimedEvent;
            return _tcs.Task;
        }

        private static async void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            _timer.Stop();
            _timer.Dispose();
            try
            {
                var res1 = await Utils.Request("https://localhost:5001/api/tasks", "GET");
                var list = JsonConvert.DeserializeObject<List<Tasks>>(res1);
                var id = list[new Random().Next(0, list.Count)].Id;
                var res2 = await Utils.Request($"https://localhost:5001/api/tasks/finish?id={id}", "GET");
                _tcs.SetResult(Convert.ToInt32(res2));
            }
            catch (Exception exception)
            {
                _tcs.SetException(exception);
            }
        }
    }
}