﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;
using Models;


namespace BusinessLogic.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly ProjectsService _projectsService;


        public ProjectsController(ProjectsService projectsService)
        {
            _projectsService = projectsService;
        }

        // GET api/projects
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Project>>> Get()
        {
            try
            {
                var projects = await _projectsService.GetProjects();
                return Ok(projects);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // GET api/projects/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Project>> Get(int id)
        {
            try
            {
                var project = await _projectsService.GetProject(id);
                return Ok(project);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // POST api/projects
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Project value)
        {
            try
            {
                await _projectsService.AddProject(value);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // PUT api/projects/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Project value)
        {
            try
            {
                await _projectsService.UpdateProject(id, value);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // DELETE api/projects/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _projectsService.DeleteProject(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }
    }
}