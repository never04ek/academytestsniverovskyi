using System;
using System.Threading.Tasks;
using BusinessLogic.Services;
using Microsoft.AspNetCore.Mvc;

namespace BusinessLogic.Controllers

{
    [Route("api/[controller]")]
    [ApiController]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }


        // GET api/linq/task1?id=5
        [HttpGet("task1")]
        public async Task<ActionResult> GetTask1(int id)
        {
            try
            {
                var res = await _linqService.GetAmountOfUserTasksInEachProject(id);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // GET api/linq/task2?id=5
        [HttpGet("task2")]
        public async Task<ActionResult> GetTask2(int id)
        {
            try
            {
                var res = await _linqService.GetUserTasks45Name(id);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // GET api/linq/task3?id=5
        [HttpGet("task3")]
        public async Task<ActionResult> GetTask3(int id)
        {
            try
            {
                var res = await _linqService.GetThisYearTasksOfUser(id);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // GET api/linq/task4
        [HttpGet("task4")]
        public async Task<ActionResult> GetTask4()
        {
            try
            {
                var res = await _linqService.GetTeamsAndUsers();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // GET api/linq/task5
        [HttpGet("task5")]
        public async Task<ActionResult> GetTask5()
        {
            try
            {
                var res = await _linqService.GetUserAndTasks();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // GET api/linq/task6?id=5
        [HttpGet("task6")]
        public async Task<ActionResult> GetTask6(int id)
        {
            try
            {
                var res = await _linqService.GetSpecUserStruct(id);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }

        // GET api/linq/task7?id=5
        [HttpGet("task7")]
        public async Task<ActionResult> GetTask7(int id)
        {
            try
            {
                var res = await _linqService.GetSpecProjectStruct(id);
                return Ok(res);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error\n" + ex);
            }
        }
        
    }
}