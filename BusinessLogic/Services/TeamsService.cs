using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Repository;
using DataAccess.UnitOfWork;
using Models;

namespace BusinessLogic.Services
{
    public class TeamsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TeamsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Team>> GetTeams()
        {
            return await _unitOfWork.Repository<Team>().GetAllAsync();
        }

        public async Task<Team> GetTeam(int id)
        {
            return await _unitOfWork.Repository<Team>().GetAsync(id);
        }

        public async Task AddTeam(Team team)
        {
             _unitOfWork.Repository<Team>().Add(team);
             await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateTeam(int id, Team team)
        {
            team.Id = id;
             _unitOfWork.Repository<Team>().Update(team);
             await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteTeam(int id)
        {
             _unitOfWork.Repository<Team>().Delete(_unitOfWork.Repository<Team>().GetAllAsync().Result.SingleOrDefault(pr => pr.Id==id));
             await _unitOfWork.SaveChangesAsync();
        }
    }
}