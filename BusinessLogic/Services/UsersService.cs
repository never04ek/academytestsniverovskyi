using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.UnitOfWork;
using Models;

namespace BusinessLogic.Services
{
    public class UsersService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UsersService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return await _unitOfWork.Repository<User>().GetAllAsync();
        }

        public async Task<User> GetUser(int id)
        {
            return await _unitOfWork.Repository<User>().GetAsync(id);
        }

        public async Task AddUser(User user)
        {
            _unitOfWork.Repository<User>().Add(user);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateUser(int id, User user)
        {
            user.Id = id;
            _unitOfWork.Repository<User>().Update(user);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteUser(int id)
        {
            _unitOfWork.Repository<User>()
                .Delete(_unitOfWork.Repository<User>().GetAllAsync().Result.SingleOrDefault(pr => pr.Id == id));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task AddUserToTeam(int userId, int teamId)
        {
            var user = GetUser(userId).Result;
            user.TeamId = teamId;
            await UpdateUser(userId, user);
        }
    }
}