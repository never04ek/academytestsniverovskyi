using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess.Repository;
using DataAccess.UnitOfWork;
using Models;

namespace BusinessLogic.Services
{
    public class ProjectsService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProjectsService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Project>> GetProjects()
        {
            return await _unitOfWork.Repository<Project>().GetAllAsync();
        }

        public async Task<Project> GetProject(int id)
        {
            return await _unitOfWork.Repository<Project>().GetAsync(id);
        }

        public async Task AddProject(Project project)
        {
            _unitOfWork.Repository<Project>().Add(project);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateProject(int id, Project project)
        {
            project.Id = id;
            _unitOfWork.Repository<Project>().Update(project);
            await _unitOfWork.SaveChangesAsync();

        }

        public async Task DeleteProject(int id)
        {
            _unitOfWork.Repository<Project>().Delete(_unitOfWork.Repository<Project>().GetAllAsync().Result
                .FirstOrDefault(pr => pr.Id == id));
            await _unitOfWork.SaveChangesAsync();

        }
    }
}