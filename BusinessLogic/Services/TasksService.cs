using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataAccess;
using DataAccess.Repository;
using DataAccess.UnitOfWork;
using Models;

namespace BusinessLogic.Services
{
    public class TasksService
    {
        private readonly IUnitOfWork _unitOfWork;

        public TasksService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Tasks>> GetTasks()
        {
            return await _unitOfWork.Repository<Tasks>().GetAllAsync();
        }

        public async Task<Tasks> GetTask(int id)
        {
            return await _unitOfWork.Repository<Tasks>().GetAsync(id);
        }

        public async Task AddTask(Tasks tasks)
        {
            _unitOfWork.Repository<Tasks>().Add(tasks);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateTask(int id, Tasks tasks)
        {
            tasks.Id = id;
            _unitOfWork.Repository<Tasks>().Update(tasks);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task DeleteTask(int id)
        {
            _unitOfWork.Repository<Tasks>()
                .Delete(_unitOfWork.Repository<Tasks>().GetAllAsync().Result.FirstOrDefault(pr => pr.Id == id));
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task FinishTask(int id)
        {
            var upd = (await _unitOfWork.Repository<Tasks>().GetAllAsync()).FirstOrDefault(task => task.Id == id);
            upd.State = TaskState.Finished;
            await UpdateTask(id, upd);
            await _unitOfWork.SaveChangesAsync();
        }
    }
}